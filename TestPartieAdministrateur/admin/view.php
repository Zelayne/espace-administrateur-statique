<?php
    require 'database.php';

    if(!empty($_GET['id']))
    {
        $id = checkInput($_GET['id']);
    }
     
    $db = Database::connect();
    $statement = $db->prepare("SELECT * FROM partie WHERE partie.idPartie = ?");
    $statement->execute(array($id));
    $partie = $statement->fetch();
    Database::disconnect();

    function checkInput($data) 
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Piccadilly Admin</title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    
    <body>
        <h1 class="text-logo"><span class="glyphicon glyphicon-cog"></span> Piccadilly Admin <span class="glyphicon glyphicon-cog"></span></h1>
         <div class="container admin">
            <div class="row">
               <div class="col-sm-6">
                    <h1><strong>Voir une partie</strong></h1>
                    <br>
                    <form>
                        <div class="form-group">
                            <label>Id de la partie :</label><?php echo '  '.$partie['idPartie'];?>
                        </div>
                      <div class="form-group">
                        <label>Nom de la partie :</label><?php echo '  '.$partie['nomPartie'];?>
                      </div>
                      <div class="form-group">
                        <label>Type de la partie :</label><?php echo '  '.$partie['type'];?>
                      </div>
                      <div class="form-group">
                        <label>Nom du sponsor :</label><?php echo '  '.$partie['nomSponsor'];?>
                      </div>
                        <div class="form-group">
                            <label>Token :</label><?php echo '  '.$partie['token'];?>
                        </div>
                      <div class="form-group">
                        <label>Cadeau :</label><?php echo '  '.$partie['cadeau'];?>
                      </div>
                        <div class="form-group">
                            <label>Nombre minimum de joueur :</label><?php echo '  '.$partie['nbMinJoueur'];?>
                        </div>
                        <div class="form-group">
                            <label>Id de la Musique :</label><?php echo '  '.$partie['idMusique'];?>
                        </div>
                        <div class="form-group">
                            <label>Id de la Note :</label><?php echo '  '.$partie['idNotes'];?>
                        </div>
                        <div class="form-group">
                            <label>Id de la Difficulté :</label><?php echo '  '.$partie['idDifficulte'];?>
                        </div>
                        <div class="form-group">
                            <label>Id de l'admin :</label><?php echo '  '.$partie['idAdministrateur'];?>
                        </div>
                    </form>
                    <br>
                    <div class="form-actions">
                      <a class="btn btn-primary" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
                    </div>
                </div> 

            </div>
        </div>   
    </body>
</html>

