<?php

    require 'database.php';

    if(!empty($_GET['id'])) 
    {
        $id = checkInput($_GET['id']);
    }

$idParte = $nomPartieError = $typeError = $nomSponsorError = $tokenError = $cadeauError = $nbMinJoueurError = $idMusiqueError = $idNotesError = $idDifficulteError = $idAdministrateurError = $nomPartie = $type = $nomSponsor = $token = $cadeau = $nbMinJoueur = $idMusique = $idNotes = $idDifficulte = $idAdministrateur = "";

    if(!empty($_POST)) {
        $idPartie = checkInput($_POST['idPartie']);
        $nomPartie = checkInput($_POST['nomPartie']);
        $type = checkInput($_POST['type']);
        $nomSponsor = checkInput($_POST['nomSponsor']);
        $token = checkInput($_POST['token']);
        $cadeau = checkInput($_POST['cadeau']);
        $nbMinJoueur = checkInput($_POST['nbMinJoueur']);
        $idMusique = checkInput($_POST['idMusique']);
        $idNotes = checkInput($_POST['idNotes']);
        $idDifficulte = checkInput($_POST['idDifficulte']);
        $idAdministrateur = checkInput($_POST['idAdministrateur']);
        $isSuccess = true;

        if (empty($idPartie)) {
            $idPartieError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($nomPartie)) {
            $nomPartieError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($type)) {
            $typeError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($nomSponsor)) {
            $nomSponsorError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($token)) {
            $tokenError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($cadeau)) {
            $cadeauError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($nbMinJoueur)) {
            $nbMinJoueurError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($idMusique)) {
            $idMusiqueError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($idNotes)) {
            $idNotesError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($idDifficulte)) {
            $idDifficulteError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }
        if (empty($idAdministrateur)) {
            $idAdministrateurError = 'Ce champ ne peut pas être vide';
            $isSuccess = false;
        }

        if ($isSuccess) {
            $db = Database::connect();

            $statement = $db->prepare("UPDATE partie  set idPartie = ?, nomPartie = ?, type = ?, nomSponsor = ?, token = ?, cadeau = ?, nbMinJoueur = ?, idMusique = ?, idNotes = ?, idDifficulte = ?, idAdministrateur = ? WHERE id = ?");
            $statement->execute(array($idPartie, $nomPartie, $type, $nomSponsor, $token, $cadeau, $nbMinJoueur, $idMusique, $idNotes, $idDifficulte, $idAdministrateur));


            $statement = $db->prepare("SELECT * FROM partie where id = ?");
            $statement->execute(array($id));
            $item = $statement->fetch();
            $name = $item['name'];
            $description = $item['description'];
            $price = $item['price'];
            $category = $item['category'];
            $image = $item['image'];

            Database::disconnect();
        }
    }

    function checkInput($data) 
    {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }

?>



<!DOCTYPE html>
<html>
    <head>
        <title>Piccadilly Admin</title>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Holtwood+One+SC' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    
    <body>
        <h1 class="text-logo"><span class="glyphicon glyphicon-cog"></span> Piccadilly Admin <span class="glyphicon glyphicon-cog"></span></h1>
         <div class="container admin">
            <div class="row">
                <div class="col-sm-6">
                    <h1><strong>Modifier une partie</strong></h1>
                    <br>
                    <form class="form" action="<?php echo 'update.php?id='.$id;?>" role="form" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="idPartie">Id de la partie:</label>
                            <input type="number" step="1"  class="form-control" id="idPartie" name="idPartie" placeholder="idPartie" value="<?php echo $idParte;?>">
                            <span class="help-inline"><?php echo $idPartieError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="nomPartie">Nom de la partie:</label>
                            <input type="text" class="form-control" id="nomPartie" name="nomPartie" placeholder="nomPartie" value="<?php echo $nomPartie;?>">
                            <span class="help-inline"><?php echo $nomPartieError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="type">Type de la partie:</label>
                            <input type="text" class="form-control" id="type" name="type" placeholder="type" value="<?php echo $type;?>">
                            <span class="help-inline"><?php echo $typeError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="nomSponsor">Nom du sponsor :</label>
                            <input type="text" class="form-control" id="nomSponsor" name="nomSponsor" placeholder="nomSponsor" value="<?php echo $nomSponsor;?>">
                            <span class="help-inline"><?php echo $nomSponsorError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="token">Token :</label>
                            <input type="text" class="form-control" id="token" name="token" placeholder="token" value="<?php echo $token;?>">
                            <span class="help-inline"><?php echo $tokenError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="cadeau">Cadeau :</label>
                            <input type="text" class="form-control" id="cadeau" name="cadeau" placeholder="cadeau" value="<?php echo $cadeau;?>">
                            <span class="help-inline"><?php echo $cadeauError;?></span>
                        </div>
                        <div class="form-group">
                            <label for="nbMinJoueur">Nombre minimum de joueurs :</label>
                            <input type="number" step="1" class="form-control" id="nbMinJoueur" name="nbMinJoueur" placeholder="nbMinJoueur" value="<?php echo $nbMinJoueur;?>">
                            <span class="help-inline"><?php echo $nbMinJoueurError;?></span>
                        </div>

                        <div class="form-group">
                            <label for="idMusique">Musique :</label>
                            <select class="form-control" id="idMusique" name="idMusique">
                                <?php
                                $db = Database::connect();
                                foreach ($db->query('SELECT * FROM musique') as $row)
                                {
                                    echo '<option value="'. $row['idMusique'] .'">'. $row['nom'] . '</option>';;
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <span class="help-inline"><?php echo $idMusiqueError;?></span>
                        </div>

                        <div class="form-group">
                            <label for="idNotes">Note :</label>
                            <select class="form-control" id="idNotes" name="idNotes">
                                <?php
                                $db = Database::connect();
                                foreach ($db->query('SELECT * FROM notes') as $row)
                                {
                                    echo '<option value="'. $row['idNotes'] .'">'. $row['contenu'] . '</option>';;
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <span class="help-inline"><?php echo $idNotesError;?></span>
                        </div>

                        <div class="form-group">
                            <label for="idDifficulte">Difficulte :</label>
                            <select class="form-control" id="idDifficulte" name="idDifficulte">
                                <?php
                                $db = Database::connect();
                                foreach ($db->query('SELECT * FROM difficulte') as $row)
                                {
                                    echo '<option value="'. $row['idDifficulte'] .'">'. $row['vitesse'] . '</option>';;
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <span class="help-inline"><?php echo $idDifficulteError;?></span>
                        </div>

                        <div class="form-group">
                            <label for="idAdministrateur">Nom de l'admin :</label>
                            <select class="form-control" id="idAdministrateur" name="idAdministrateur">
                                <?php
                                $db = Database::connect();
                                foreach ($db->query('SELECT * FROM administrateur') as $row)
                                {
                                    echo '<option value="'. $row['idAdministrateur'] .'">'. $row['username'] . '</option>';;
                                }
                                Database::disconnect();
                                ?>
                            </select>
                            <span class="help-inline"><?php echo $idAdministrateurError;?></span>
                        </div>

                        <br>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> Modifier</button>
                            <a class="btn btn-primary" href="index.php"><span class="glyphicon glyphicon-arrow-left"></span> Retour</a>
                        </div>
                    </form>
                </div>

            </div>
         </div>
    </body>
</html>
